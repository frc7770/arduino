/******************************************************************
 * Infinite Voltage FRC 7770
 * 
 * This file is a bench test for the REV Robotics Color Sensor
 * Data Sheet for APDS-9151 sensor:
 * https://docs.broadcom.com/doc/APDS-9151-DS
 * 
 * This project was based initially on 
 * SparkFun Inventor's Kit
 * Example sketch 03 -- RGB LED
 * Visit http://learn.sparkfun.com/products/2 for SIK information.
 * Visit http://www.arduino.cc to learn about the Arduino.
 * 
 *****************************************************************/

// This library allows you to communicate with I2C / TWI devices
#include <Wire.h>

// Multicolor LED
const int RED_PIN = 9;
const int GREEN_PIN = 10;
const int BLUE_PIN = 11;
const int PROX_PIN = 13;

// Color Sensor
// REV Robotics Color Sensor V3
// APDS-9151
// see page 16 of https://docs.broadcom.com/doc/APDS-9151-DS
const int I2C_ADDRESS = 82; //(0x52);
const int REGISTER_RED0 = 19; //(0x13);
const int REGISTER_RED1 = 20; //(0x14);
const int REGISTER_RED2 = 21; //(0x15);
const int REGISTER_GREEN0 = 13; //(0x0D);
const int REGISTER_GREEN1 = 14; //(0x0E);
const int REGISTER_GREEN2 = 15; //(0x0F);
const int REGISTER_BLUE0 = 16; //(0x10);
const int REGISTER_BLUE1 = 17; //(0x11);
const int REGISTER_BLUE2 = 18; //(0x12);
const int REGISTER_ID = 6; //(0x06);
const int REGISTER_ID_1 = 194; //(0xC2);
const int REGISTER_STATUS = 7; //(0x07);
const int REGISTER_MAIN_CTL = 0; //(0x00);
const int STATUS = 32;

// Color calibration
int Glow = 1000;
int Ghigh = -1;
int Blow = 1000;
int Bhigh = -1;
int Rlow = 1000;
int Rhigh = -1;


/******************************************************************
 * void setup()
 * This function runs at the beginning
/*****************************************************************/
void setup()	//Configure the Arduino pins to be outputs to drive the LEDs
{
  // Multicolor LED setup
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  // Color Sensor Setup
  // Initialize Serial port
  Wire.begin(); // Wire communication begin
  Serial.begin(9600);
  Serial.println("Starting");
  initSensor();

  // Initialize proximity LED
  pinMode(PROX_PIN, OUTPUT);
}


/******************************************************************
 * void loop()
 * This function runs throughout the execution
/*****************************************************************/
void loop()
{
  // read red
  int red = getRed();
  Serial.print("Red: ");
  Serial.print(red);
  // read green
  int green = getGreen();
  Serial.print("  Green: ");
  Serial.print(green);
  // read blue
  int blue = getBlue();
  Serial.print("  Blue: ");
  Serial.print(blue);
  // read proximity
  Serial.print("  PS: ");
  int prox = getPS();
  Serial.print(prox);

  // Calibration output
  Serial.print(" Rlow:");
  Serial.print(Rlow);
  Serial.print(" Rhigh:");
  Serial.print(Rhigh);
  Serial.print(" Glow:");
  Serial.print(Glow);
  Serial.print(" Ghigh:");
  Serial.print(Ghigh);
  Serial.print(" Blow:");
  Serial.print(Blow);
  Serial.print(" Bhigh:");
  Serial.print(Bhigh);

  Serial.println();

  // set the led color
  setColor(red,green,blue);

  // Set the proximity LED
  setProximity(prox);
}


/******************************************************************
 * void getGreen()
 * This function gets the 0-255 green component of RGB
/*****************************************************************/
int getGreen() {
  int color = getColor(REGISTER_GREEN0,REGISTER_GREEN1,REGISTER_GREEN1);
  
  // Use real time calibration
  if (color < Glow) {
    Glow = color;
  }
  if (color > Ghigh) {
    Ghigh = color;
  }
  //int rawColor = map(color,Glow,Ghigh,0,255);
  
  // Calibrate manually
  int low = 1;
  int high = 8;
  int rawColor = map(color,low,high,0,255);

  // return the value and make sure it fits within 0-255 range
  return constrain(rawColor,0,255);
}



/******************************************************************
 * void getBlue()
 * This function gets the 0-255 blue component of RGB
/*****************************************************************/
int getBlue() {
  int color = getColor(REGISTER_BLUE0,REGISTER_BLUE1,REGISTER_BLUE2);
  
  // Use real time calibration
  if (color < Blow) {
    Blow = color;
  }
  if (color > Bhigh) {
    Bhigh = color;
  }
  //int rawColor = map(color,Blow,Bhigh,0,255);
  
  // Calibrate manually
  int low = 0;
  int high = 3;
  int rawColor = map(color,low,high,0,255);

  // return the value and make sure it fits within 0-255 range
  return constrain(rawColor,0,255);
}


/******************************************************************
 * void getRed()
 * This function gets the 0-255 red component of RGB
/*****************************************************************/
int getRed() {
  int color = getColor(REGISTER_RED0,REGISTER_RED1,REGISTER_RED2);
  
  // Use real time calibration
  if (color < Rlow) {
    Rlow = color;
  }
  if (color > Rhigh) {
    Rhigh = color;
  }
  //int rawColor = map(color,Rlow,Rhigh,0,255);
  
  // Calibrate manually
  int low = 0;
  int high = 6;
  int rawColor = map(color,low,high,0,255);

  // return the value and make sure it fits within 0-255 range
  return constrain(rawColor,0,255);
}



/******************************************************************
 * void getColor()
 * This function the values of three registries for each color
 * from the sensor and turns them into a color value.
/*****************************************************************/
int getColor(byte registry1, byte registry2, byte registry3) {
  byte reg1 = ReadRegistryValue(registry1);
  byte reg2 = ReadRegistryValue(registry2);
  byte reg3 = ReadRegistryValue(registry3);
  
  // Trying different combinations to figure out what works best
  //int val = (reg1 & 0xFF);
  //int val = ((reg2 & 0xFF) << 8) | (reg1 & 0xFF);
  //int val = ((reg3 & 0x7F) << 16) | ((reg2 & 0xFF) << 8) | (reg1 & 0xFF);
  //byte val = ((reg3 & 0x0f) << 4); // | 
  
  // this one is not too bad, but washed out
  //byte val = ((reg2 & 0x0F) << 4) | ((reg1 & 0xF0) >> 4) ;
  
  // this seems to work well
  byte val = reg2;
  
  return val;
}


/******************************************************************
 * void getPS()
 * This function gets the proximity value
 * See page 22 of https://docs.broadcom.com/doc/APDS-9151-DS
/*****************************************************************/
int getPS() {
  byte reg1 = ReadRegistryValue(8);
  byte reg2 = ReadRegistryValue(9);
  reg2 = reg2 & 0x07;
  //int val = ((reg2 & 0x07) << 8) | (reg1 & 0xff) ;
  return (reg2*256)+reg1;
}

void setProximity(int prox) {
  if (prox > 10) { // Senses things within 9 centimeters
    digitalWrite(PROX_PIN, HIGH);
  }
  else {
    digitalWrite(PROX_PIN, LOW);
  }
}


/******************************************************************
 * void initSensor()
 * This function starts up the sensor connection
 * See Data Sheet https://docs.broadcom.com/doc/APDS-9151-DS
/*****************************************************************/
bool initSensor() {  
  Serial.println("Init sensor");
  byte id;

  // Check if the I2C device is active
  Wire.beginTransmission(I2C_ADDRESS);
  byte error = Wire.endTransmission();
  if (error == 0) {
    Serial.print("Device active on 0x");
    Serial.println(I2C_ADDRESS, HEX);
  } else {
    Serial.print("Device not found on 0x");
    Serial.println(I2C_ADDRESS, HEX);
    return false;
  }
  
  /// Initialize I2C 
  //Check ID of Sensor   
  id = ReadRegistryValue(REGISTER_ID);
  Serial.print("ID: ");
  Serial.println(id);
  //< Read ID register and check against known values for APDS-9151 
  if( id != REGISTER_ID_1 ) {
    return false;
  }

  // check status
  id = ReadRegistryValue(REGISTER_STATUS);
  Serial.print("STATUS: ");
  Serial.println(id);
  if( id != STATUS ) {
    // not sure why this doesn't return the value we expect so commenting out
     //return false;
  }

  // set main controls - turn on RGB and proximity
  SetControlRegistry (7);

  return true;
}


/******************************************************************
 * void SetControlRegistry()
 * This function sets control flags.  Usually we turn on RGB and
 * proximity sensing.
 * See page 17 of https://docs.broadcom.com/doc/APDS-9151-DS
/*****************************************************************/
void SetControlRegistry(byte iVal) {
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(REGISTER_MAIN_CTL);
  Wire.write(iVal);
  Wire.endTransmission();
}


/******************************************************************
 * void ReadRegistryValue()
 * This function reads the current value of a registry of the I2C
 * device.
/*****************************************************************/
byte ReadRegistryValue(int iReg)
{
    byte id;

    // Request a registry value
    Wire.beginTransmission(I2C_ADDRESS);
    Wire.write(iReg);
    Wire.endTransmission();
    
    // Read the returned value as one byte
    Wire.requestFrom(I2C_ADDRESS,1);
    while (Wire.available()) {
       id = Wire.read();
    }

    return id;
}


/******************************************************************
 * void setColor()
 * This function displays sets the reb, green, and blue components 
 * of the multi color LED
/*****************************************************************/
void setColor(int red, int green, int blue) {

  // "send" intensity values to the Red, Green, Blue Pins using analogWrite()
  analogWrite(RED_PIN, red);
  analogWrite(GREEN_PIN, green);
  analogWrite(BLUE_PIN, blue);
}
